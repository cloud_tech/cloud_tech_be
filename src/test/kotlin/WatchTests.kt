import com.gitlab.medelyan.app.config.MainConfig
import com.gitlab.medelyan.dao.WatchDao
import com.gitlab.medelyan.dao.WatchSampleDao
import com.gitlab.medelyan.dto.ActivityDto
import com.gitlab.medelyan.generated.tables.pojos.Watch
import com.gitlab.medelyan.generated.tables.pojos.WatchSample
import com.gitlab.medelyan.service.WatchService
import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.flywaydb.core.Flyway
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import java.time.Duration
import java.time.Instant

@SpringBootTest(classes = [MainConfig::class])
@AutoConfigureEmbeddedDatabase
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class WatchTests {

    @Autowired
    private lateinit var flyway: Flyway

    @Autowired
    private lateinit var watchDao: WatchDao

    @Autowired
    private lateinit var watchSampleDao: WatchSampleDao

    @Autowired
    private lateinit var watchService: WatchService

    @BeforeEach
    fun init() {
        flyway.migrate()
    }

    @AfterEach
    fun clean() {
        flyway.clean()
    }

    @Test
    fun `empty history -- empty activity`() {
        val watch = Watch(issuerId = 0, issuerToken = "", watcheeId = 1)
        watchDao.insert(watch)

        val actualActivity = watchService.getActivityOf(watch.id!!)
        val expectedActivity = listOf<ActivityDto>()

        assertEquals(expectedActivity, actualActivity)
    }

    @Test
    fun `same samples are squashed`() {
        val watch = Watch(issuerId = 0, issuerToken = "", watcheeId = 1)
        watchDao.insert(watch)

        val now = Instant.now()
        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = true,
                at = now
            )
        )
        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = true,
                at = now.plus(Duration.ofMinutes(1))
            )
        )
        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = true,
                at = now.plus(Duration.ofMinutes(2))
            )
        )

        val actualActivity = watchService.getActivityOf(watch.id!!)
        val expectedActivity = listOf(ActivityDto(online = true, duration = Duration.ofMinutes(2)))

        assertEquals(expectedActivity, actualActivity)
    }

    @Test
    fun `different samples work fine`() {
        val watch = Watch(issuerId = 0, issuerToken = "", watcheeId = 1)
        watchDao.insert(watch)

        val now = Instant.now()
        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = true,
                at = now
            )
        )
        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = true,
                at = now.plus(Duration.ofMinutes(1))
            )
        )
        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = true,
                at = now.plus(Duration.ofMinutes(2))
            )
        )

        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = false,
                at = now.plus(Duration.ofMinutes(3))
            )
        )
        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = false,
                at = now.plus(Duration.ofMinutes(4))
            )
        )

        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = true,
                at = now.plus(Duration.ofMinutes(5))
            )
        )
        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = true,
                at = now.plus(Duration.ofMinutes(6))
            )
        )
        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = true,
                at = now.plus(Duration.ofMinutes(7))
            )
        )

        val actualActivity = watchService.getActivityOf(watch.id!!)
        val expectedActivity = listOf(
            ActivityDto(online = true, duration = Duration.ofMinutes(3)),
            ActivityDto(online = false, duration = Duration.ofMinutes(2)),
            ActivityDto(online = true, duration = Duration.ofMinutes(2))
        )

        assertEquals(expectedActivity, actualActivity)
    }

    @Test
    fun `empty activity is removed`() {
        val watch = Watch(issuerId = 0, issuerToken = "", watcheeId = 1)
        watchDao.insert(watch)

        val now = Instant.now()
        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = true,
                at = now
            )
        )
        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = true,
                at = now.plus(Duration.ofMinutes(1))
            )
        )
        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = true,
                at = now.plus(Duration.ofMinutes(2))
            )
        )

        watchSampleDao.insert(
            WatchSample(
                watchId = watch.id,
                online = false,
                at = now.plus(Duration.ofMinutes(3))
            )
        )

        val actualActivity = watchService.getActivityOf(watch.id!!)
        val expectedActivity = listOf(
            ActivityDto(online = true, duration = Duration.ofMinutes(3))
        )

        assertEquals(expectedActivity, actualActivity)
    }
}