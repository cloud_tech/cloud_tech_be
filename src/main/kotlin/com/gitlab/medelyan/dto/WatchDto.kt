package com.gitlab.medelyan.dto

data class WatchDto(val id: Int)
