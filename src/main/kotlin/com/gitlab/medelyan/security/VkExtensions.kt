package com.gitlab.medelyan.security

import org.springframework.security.oauth2.client.registration.ClientRegistration

const val VK_PROVIDER_NAME = "vk"

fun ClientRegistration.isVk(): Boolean = clientName == VK_PROVIDER_NAME
