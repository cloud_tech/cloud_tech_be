package com.gitlab.medelyan.dto

import java.time.Duration

data class ActivityDto(
    val online: Boolean,
    val duration: Duration
)