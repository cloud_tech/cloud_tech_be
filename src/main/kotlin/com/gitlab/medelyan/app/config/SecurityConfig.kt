package com.gitlab.medelyan.app.config

import com.gitlab.medelyan.security.CustomOAuth2UserService
import com.gitlab.medelyan.security.CustomRequestEntityConverter
import com.gitlab.medelyan.security.CustomTokenResponseConverter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.FormHttpMessageConverter
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.web.servlet.invoke
import org.springframework.security.oauth2.client.endpoint.DefaultAuthorizationCodeTokenResponseClient
import org.springframework.security.oauth2.core.http.converter.OAuth2AccessTokenResponseHttpMessageConverter
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler
import org.springframework.security.web.csrf.CsrfTokenRepository
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.web.client.RestTemplate
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter

@Configuration
class SecurityConfig(
    private val mainConfig: MainConfig,
    private val tokenRepository: CsrfTokenRepository
) : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity?) {
        http {
            authorizeRequests {
                authorize("/csrf", permitAll)
                authorize(AntPathRequestMatcher("/**", "OPTIONS"), permitAll)
                authorize(anyRequest, authenticated)
            }
            logout {
                logoutSuccessHandler = HttpStatusReturningLogoutSuccessHandler()
            }
            csrf {
                csrfTokenRepository = tokenRepository
            }
            oauth2Login {
                tokenEndpoint {
                    accessTokenResponseClient = accessTokenResponseClient()
                }
                userInfoEndpoint {
                    userService = CustomOAuth2UserService()
                }
                loginPage = mainConfig.loginPageUrl
                defaultSuccessUrl(defaultSuccessUrl = mainConfig.homePageUrl, alwaysUse = true)
            }
        }
    }

    private fun accessTokenResponseClient(): DefaultAuthorizationCodeTokenResponseClient {
        val client = DefaultAuthorizationCodeTokenResponseClient()
        // custom User class
        client.setRequestEntityConverter(CustomRequestEntityConverter())

        // fix issues with access token
        val tokenConverter = OAuth2AccessTokenResponseHttpMessageConverter()
        tokenConverter.setTokenResponseConverter(CustomTokenResponseConverter())
        val restTemplate = RestTemplate(listOf(FormHttpMessageConverter(), tokenConverter))

        client.setRestOperations(restTemplate)
        return client
    }

    @Bean
    fun corsFilter(): CorsFilter {
        val config = CorsConfiguration()
        config.allowCredentials = true
        config.allowedOrigins = mainConfig.cors.allowedHosts.map { it.withoutLastSlash() }
        config.allowedHeaders = listOf("*")
        config.allowedMethods = listOf("GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH")

        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", config)
        return CorsFilter(source)
    }
}

private fun String.withoutLastSlash(): String {
    return substringBeforeLast('/')
}