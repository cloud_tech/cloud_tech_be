CREATE TABLE watch (
    id BIGSERIAL NOT NULL,
    issuer_id INT NOT NULL,
    issuer_token TEXT NOT NULL,
    watchee_id INT NOT NULL,
    CONSTRAINT PK_watch PRIMARY KEY (id),
    UNIQUE (issuer_id, issuer_token, watchee_id)
);

CREATE TABLE watch_sample (
    id BIGSERIAL NOT NULL,
    watch_id BIGINT NOT NULL,
    online BOOLEAN NOT NULL,
    at TIMESTAMP NOT NULL,
    CONSTRAINT PK_watch_sample PRIMARY KEY (id),
    CONSTRAINT watch_sample_watch FOREIGN KEY (watch_id) REFERENCES watch (id)
);
