package com.gitlab.medelyan.controller

import com.gitlab.medelyan.dto.CsrfTokenDto
import org.springframework.security.web.csrf.CsrfTokenRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest

@RestController
class CsrfController(
    private val csrfTokenRepository: CsrfTokenRepository
) {

    @GetMapping("csrf")
    fun getToken(request: HttpServletRequest): CsrfTokenDto {
        val token = csrfTokenRepository.loadToken(request)
        return CsrfTokenDto(
            headerName = token.headerName,
            token = token.token
        )
    }
}