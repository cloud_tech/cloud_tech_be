package com.gitlab.medelyan.security

import org.springframework.core.convert.converter.Converter
import org.springframework.security.oauth2.core.OAuth2AccessToken
import org.springframework.security.oauth2.core.endpoint.MapOAuth2AccessTokenResponseConverter
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames.TOKEN_TYPE

class CustomTokenResponseConverter : Converter<Map<String, String>, OAuth2AccessTokenResponse> {
    override fun convert(source: Map<String, String>): OAuth2AccessTokenResponse? {
        val sourceMap = source.toMutableMap()

        // [SM] VK does not set this, dammit!
        if (sourceMap[TOKEN_TYPE] == null) {
            sourceMap[TOKEN_TYPE] = OAuth2AccessToken.TokenType.BEARER.value
        }
        return MapOAuth2AccessTokenResponseConverter().convert(sourceMap)
    }
}