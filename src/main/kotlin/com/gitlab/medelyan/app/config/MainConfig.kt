package com.gitlab.medelyan.app.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import javax.validation.constraints.NotBlank

@Configuration
@EnableWebMvc
@EnableAutoConfiguration
@ComponentScan("com.gitlab.medelyan")
@ConfigurationPropertiesScan("com.gitlab.medelyan")
@ConfigurationProperties("app")
class MainConfig {
    @NotBlank
    lateinit var homePageUrl: String

    @NotBlank
    lateinit var loginPageUrl: String
    lateinit var cors: CorsConfig

    @Bean
    fun httpSessionCsrfTokenRepository(): HttpSessionCsrfTokenRepository {
        return HttpSessionCsrfTokenRepository()
    }

    @Bean
    fun objectMapper(): ObjectMapper {
        val mapper = ObjectMapper()
        mapper.registerModule(JavaTimeModule())
        mapper.registerModule(KotlinModule())
        return mapper
    }
}
