FROM openjdk:11-jre-slim

ENV PROJECT_HOME=/opt/cloud-tech
COPY build/libs/*.jar $PROJECT_HOME/cloud-tech.jar

WORKDIR $PROJECT_HOME

ENTRYPOINT ["java", "-XX:+PrintFlagsFinal", "-jar", "cloud-tech.jar"]
