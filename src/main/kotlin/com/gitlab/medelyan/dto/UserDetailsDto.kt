package com.gitlab.medelyan.dto

data class UserDetailsDto(
    val id: Int,
    val firstName: String,
    val lastName: String,
    val photoUrl: String,
    val isWatched: Boolean = false,
    val activity: List<ActivityDto> = listOf()
)