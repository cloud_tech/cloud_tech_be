package com.gitlab.medelyan.controller

import com.gitlab.medelyan.dao.WatchDao
import com.gitlab.medelyan.dto.UserDetailsDto
import com.gitlab.medelyan.security.VkOAuth2User
import com.gitlab.medelyan.service.VkService
import com.gitlab.medelyan.service.WatchService
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class UserController(
    val vkService: VkService,
    val watchDao: WatchDao,
    val watchService: WatchService
) {

    @GetMapping("me")
    fun me(@AuthenticationPrincipal user: VkOAuth2User): UserDetailsDto {
        return vkService.userDetails(user)
    }

    @GetMapping("friends")
    fun getFriends(@AuthenticationPrincipal user: VkOAuth2User): List<UserDetailsDto> {
        return vkService.userFriends(user)
            .map {
                val watch = watchDao.findByIssuerAndWatchee(user.id, it.id)
                UserDetailsDto(
                    id = it.id,
                    firstName = it.firstName,
                    lastName = it.lastName,
                    photoUrl = it.photoUrl,
                    isWatched = watch != null,
                    activity = if (watch != null) watchService.getActivityOf(watch.id!!) else listOf()
                )
            }
    }
}
