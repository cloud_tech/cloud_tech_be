package com.gitlab.medelyan.app.config

import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling

// [SM] May become ConditionalOn in future
@Configuration
@EnableScheduling
class SchedulingConfig {
}