package com.gitlab.medelyan.service

import com.gitlab.medelyan.dao.WatchDao
import com.gitlab.medelyan.dao.WatchSampleDao
import com.gitlab.medelyan.dto.ActivityDto
import com.gitlab.medelyan.generated.tables.pojos.Watch
import com.gitlab.medelyan.generated.tables.pojos.WatchSample
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Duration
import java.time.Duration.ZERO
import java.time.Instant

@Service
class WatchService(
    private val watchDao: WatchDao,
    private val watchSampleDao: WatchSampleDao,
    private val vkService: VkService,
    @Value("\${app.watch.online-window:15m}")
    private val onlineWindow: Duration
) {

    @Transactional
    @Scheduled(cron = "0 * * * * *")
    @Synchronized
    fun watch() {
        watchDao.findAll().forEach { updateWatch(it) }
    }

    private fun updateWatch(watch: Watch) {
        val now = Instant.now()
        val seenAt = vkService.lastSeen(watch)

        val sample = WatchSample(
            watchId = watch.id,
            at = now,
            online = seenAt.plus(onlineWindow).isAfter(now)
        )

        watchSampleDao.insert(sample)
    }

    fun getActivityOf(watchId: Long): List<ActivityDto> {
        val samples = watchSampleDao.fetchLastThreeDays(watchId)
        return squashSamples(samples)
    }

    private fun squashSamples(samples: List<WatchSample>): List<ActivityDto> {
        if (samples.isEmpty()) return listOf()

        val activity = mutableListOf<ActivityDto>()
        var previous: WatchSample? = null
        var duration: Duration = ZERO

        for (s in samples) {
            if (previous == null) {
                previous = s
                continue
            }

            duration += Duration.between(previous.at, s.at)
            if (previous.online != s.online) {
                activity.add(ActivityDto(previous.online!!, duration))
                duration = ZERO
            }

            previous = s
        }

        activity.add(ActivityDto(previous!!.online!!, duration))
        activity.removeIf { (_, duration) -> duration == ZERO }

        return activity
    }
}