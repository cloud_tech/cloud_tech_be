package com.gitlab.medelyan.app.config

import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
class CorsConfig(
    val allowedHosts: List<String>
) 