package com.gitlab.medelyan.dao

import com.gitlab.medelyan.generated.tables.daos.WatchDaoBase
import com.gitlab.medelyan.generated.tables.pojos.Watch
import com.gitlab.medelyan.generated.tables.references.WATCH
import org.jooq.Configuration
import org.jooq.DSLContext
import org.springframework.stereotype.Repository

@Repository
class WatchDao(
    private val dsl: DSLContext,
    configuration: Configuration
) : WatchDaoBase(configuration) {

    fun findByIssuerAndWatchee(issuerId: Int, watcheeId: Int): Watch? {
        return dsl
            .selectFrom(WATCH)
            .where(WATCH.ISSUER_ID.eq(issuerId))
            .and(WATCH.WATCHEE_ID.eq(watcheeId))
            .fetchOneInto(Watch::class.java)
    }
}