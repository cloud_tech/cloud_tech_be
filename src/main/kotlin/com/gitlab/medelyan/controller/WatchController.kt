package com.gitlab.medelyan.controller

import com.gitlab.medelyan.dao.WatchDao
import com.gitlab.medelyan.dto.WatchDto
import com.gitlab.medelyan.generated.tables.pojos.Watch
import com.gitlab.medelyan.security.VkOAuth2User
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class WatchController(
    private val watchDao: WatchDao
) {

    @PostMapping("watch")
    @Transactional
    fun watch(@AuthenticationPrincipal user: VkOAuth2User, @RequestBody watchDto: WatchDto) {
        watchDao.insert(
            Watch(
                issuerId = user.id,
                issuerToken = user.token,
                watcheeId = watchDto.id
            )
        )
    }
}