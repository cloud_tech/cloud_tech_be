import nu.studer.gradle.jooq.JooqEdition.OSS
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jooq.meta.jaxb.ForcedType
import org.jooq.meta.jaxb.MatcherRule
import org.jooq.meta.jaxb.MatcherTransformType.PASCAL
import org.jooq.meta.jaxb.Matchers
import org.jooq.meta.jaxb.MatchersTableType
import org.jooq.meta.jaxb.Strategy
import java.io.ByteArrayOutputStream

plugins {
    id("org.springframework.boot") version "2.4.1"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
    kotlin("jvm") version "1.4.21"
    kotlin("plugin.spring") version "1.4.21"
    id("org.flywaydb.flyway") version "7.4.0"
    id("nu.studer.jooq") version "5.2"
}

group = "com.gitlab.medelyan"
version = gitLastTag()
java.sourceCompatibility = JavaVersion.VERSION_11

val generatedDir = "build/generated/jooq"

sourceSets.main {
    withConvention(KotlinSourceSet::class) {
        kotlin.srcDirs += file(generatedDir)
    }
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-jdbc")
    implementation("org.springframework.boot:spring-boot-starter-jooq")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-client:2.4.0")

    runtimeOnly("org.postgresql:postgresql:42.2.18")
    implementation("org.flywaydb:flyway-core")
    implementation("org.jooq:jooq")
    jooqGenerator("org.postgresql:postgresql:42.2.18")
    implementation("org.flywaydb:flyway-core")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation(group = "com.fasterxml.jackson.datatype", name = "jackson-datatype-jsr310", version = "2.12.0")
    implementation(group = "com.fasterxml.jackson.module", name = "jackson-module-kotlin", version = "2.12.0")

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }

    testImplementation(group = "io.zonky.test", name = "embedded-database-spring-test", version = "1.6.1")
    testImplementation(group = "com.nhaarman.mockitokotlin2", name = "mockito-kotlin", version = "2.2.0")

    implementation("com.vk.api:sdk:1.0.8") {
        exclude(group = "org.slf4j")
        exclude(group = "org.apache.logging.log4j")
    }
    implementation(group = "org.hibernate.validator", name = "hibernate-validator", version = "6.1.6.Final")

}

tasks.withType<Test> {
    useJUnitPlatform()
}

val pgHost = System.getenv("POSTGRES_HOST") ?: "localhost"
val pgPort = System.getenv("POSTGRES_PORT") ?: "5432"
val pgUser = System.getenv("POSTGRES_USER") ?: "user"
val pgPassword = System.getenv("POSTGRES_PASSWORD") ?: "z42kRdL2XEtK7pOS0taM"
val pgDb = System.getenv("POSTGRES_DB") ?: "cloud-tech-db"

flyway {
    url = "jdbc:postgresql://$pgHost:$pgPort/$pgDb"
    user = pgUser
    password = pgPassword
    schemas = arrayOf("public")
    driver = "org.postgresql.Driver"
}

jooq {
    version.set("3.14.4")
    edition.set(OSS)

    configurations {
        create("main") {
            generateSchemaSourceOnCompilation.set(true)

            jooqConfiguration.apply {
                logging = org.jooq.meta.jaxb.Logging.WARN
                jdbc.apply {
                    driver = "org.postgresql.Driver"
                    url = "jdbc:postgresql://$pgHost:$pgPort/cloud-tech-db"
                    user = pgUser
                    password = pgPassword
                }
                generator.apply {
                    strategy = Strategy().apply {
                        matchers = Matchers().apply {
                            tables = listOf(MatchersTableType().apply {
                                tableClass = MatcherRule().apply {
                                    transform = PASCAL
                                    expression = "\$0_TABLE"
                                }
                                daoClass = MatcherRule().apply {
                                    transform = PASCAL
                                    expression = "\$0_DAO_BASE"
                                }
                            })
                        }
                    }
                    name = "org.jooq.codegen.KotlinGenerator"
                    database.apply {
                        name = "org.jooq.meta.postgres.PostgresDatabase"
                        inputSchema = "public"
                        includes = ".*"
                        excludes = "flyway_schema_history"
                        withForcedTypes(
                            ForcedType().apply {
                                includeTypes = "TIMESTAMP"
                                userType = "java.time.Instant"
                                converter = "com.gitlab.medelyan.dao.LocalDateTimeToInstantConverter"
                            }
                        )
                    }
                    generate.apply {
                        isRelations = true
                        isDeprecated = false
                        isRecords = true
                        isPojos = true
                        // isImmutablePojos = false
                        isFluentSetters = true
                        isSpringAnnotations = true
                        isDaos = true
                        isPojosEqualsAndHashCode = true
                    }
                    target.apply {
                        packageName = "com.gitlab.medelyan.generated"
                        directory = generatedDir
                    }
                }
            }
        }
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "11"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "11"
    }
}

fun gitLastTag(): String {
    return execute("git", "describe", "--tags", "--always", "--first-parent")
}

fun execute(vararg args: String): String {
    val out = ByteArrayOutputStream()
    exec {
        commandLine(*args)
        standardOutput = out
    }
    return out.toString().trim()
}
