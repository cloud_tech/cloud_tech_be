package com.gitlab.medelyan.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest
import org.springframework.security.oauth2.core.user.DefaultOAuth2User
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority

class VkOAuth2User(
    val id: Int,
    val token: String,
    private val name: String,
    authorities: MutableCollection<out GrantedAuthority>?,
    attributes: MutableMap<String, Any>?,
    nameAttributeKey: String?
) : DefaultOAuth2User(authorities, attributes, nameAttributeKey) {

    companion object {
        fun parse(request: OAuth2UserRequest): VkOAuth2User {
            return when (request.clientRegistration.clientName.toLowerCase()) {
                "vk" -> parseVk(request)
                else -> error("Unknown provider ${request.clientRegistration.clientName}")
            }
        }

        private fun parseVk(request: OAuth2UserRequest): VkOAuth2User = VkOAuth2User(
            id = (getAttributes(request)["user_id"] as String).toInt(),
            token = request.accessToken.tokenValue,
            name = getAttributes(request)["user_id"] as String,
            authorities = getAuthorities(request),
            attributes = getAttributes(request),
            nameAttributeKey = getNameAttributeKey(request)
        )

        private fun getAttributes(request: OAuth2UserRequest): MutableMap<String, Any> {
            return request.additionalParameters
        }

        private fun getAuthorities(request: OAuth2UserRequest): MutableSet<GrantedAuthority> {
            return linkedSetOf(OAuth2UserAuthority(getAttributes(request)))
        }

        private fun getNameAttributeKey(request: OAuth2UserRequest): String {
            return request.clientRegistration.providerDetails.userInfoEndpoint.userNameAttributeName
        }
    }

    override fun getName(): String = name
}
