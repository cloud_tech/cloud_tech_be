package com.gitlab.medelyan.security

import org.springframework.http.RequestEntity
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequestEntityConverter
import org.springframework.util.MultiValueMap

class CustomRequestEntityConverter : OAuth2AuthorizationCodeGrantRequestEntityConverter() {
    override fun convert(request: OAuth2AuthorizationCodeGrantRequest): RequestEntity<*>? {
        if (!request.clientRegistration.isVk()) {
            return super.convert(request)
        }

        val entity = super.convert(request)
        val params = entity?.body as MultiValueMap<String, String>
        // params["v"] = "5.126"
        return RequestEntity(params, entity.headers, entity.method, entity.url)
    }
}