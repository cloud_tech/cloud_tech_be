package com.gitlab.medelyan.dao

import com.gitlab.medelyan.generated.tables.daos.WatchSampleDaoBase
import com.gitlab.medelyan.generated.tables.pojos.WatchSample
import com.gitlab.medelyan.generated.tables.references.WATCH_SAMPLE
import org.jooq.Configuration
import org.jooq.DSLContext
import org.springframework.stereotype.Repository
import java.time.Duration
import java.time.Instant

@Repository
class WatchSampleDao(
    private val dsl: DSLContext,
    configuration: Configuration
) : WatchSampleDaoBase(configuration) {

    fun fetchLastThreeDays(watchId: Long): List<WatchSample> {
        val weekAgo = Instant.now().minus(Duration.ofDays(3))
        return dsl
            .selectFrom(WATCH_SAMPLE)
            .where(WATCH_SAMPLE.WATCH_ID.eq(watchId))
            .and(WATCH_SAMPLE.AT.ge(weekAgo))
            .orderBy(WATCH_SAMPLE.AT.asc())
            .fetchInto(WatchSample::class.java)
    }
}