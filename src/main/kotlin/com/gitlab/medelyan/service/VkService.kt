package com.gitlab.medelyan.service

import com.gitlab.medelyan.dto.UserDetailsDto
import com.gitlab.medelyan.generated.tables.pojos.Watch
import com.gitlab.medelyan.security.VkOAuth2User
import com.vk.api.sdk.client.VkApiClient
import com.vk.api.sdk.client.actors.UserActor
import com.vk.api.sdk.httpclient.HttpTransportClient
import com.vk.api.sdk.objects.enums.UsersNameCase.NOMINATIVE
import com.vk.api.sdk.objects.users.Fields.FIRST_NAME_NOM
import com.vk.api.sdk.objects.users.Fields.LAST_NAME_NOM
import com.vk.api.sdk.objects.users.Fields.LAST_SEEN
import com.vk.api.sdk.objects.users.Fields.PHOTO_50
import com.vk.api.sdk.objects.users.UserFull
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class VkService {

    private val vk: VkApiClient = VkApiClient(HttpTransportClient.getInstance())

    fun userDetails(user: VkOAuth2User): UserDetailsDto {
        val me = vk.users().get(user.actor)
            .fields(
                FIRST_NAME_NOM,
                LAST_NAME_NOM,
                PHOTO_50
            )
            .execute().first()
        me.id = user.id
        return me.toDto()
    }

    fun userFriends(user: VkOAuth2User): List<UserDetailsDto> {
        val friendIds = vk.friends().get(user.actor).execute().items
        val r = vk.users().get(user.actor)
            .userIds(friendIds.map { it.toString() })
            .nameCase(NOMINATIVE)
            .fields(
                FIRST_NAME_NOM,
                LAST_NAME_NOM,
                PHOTO_50
            )
        val friends = r.execute()
        for (i in friendIds.indices) {
            friends[i].id = friendIds[i]
        }
        return friends.map { it.toDto() }
    }

    fun lastSeen(watch: Watch): Instant {
        val actor = UserActor(watch.issuerId, watch.issuerToken)
        val seenAtUnix = vk.users().get(actor)
            .fields(LAST_SEEN)
            .userIds(watch.watcheeId.toString())
            .execute()
            .first()
            .lastSeen
            .time

        return Instant.ofEpochSecond(seenAtUnix.toLong())
    }
}

private val VkOAuth2User.actor
    get() = UserActor(id, token)

private fun UserFull.toDto(): UserDetailsDto = UserDetailsDto(
    id = id,
    firstName = firstName,
    lastName = lastName,
    photoUrl = photo50.toString()
)