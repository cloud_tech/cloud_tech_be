package com.gitlab.medelyan.dto

data class CsrfTokenDto(
    val headerName: String,
    val token: String
)
